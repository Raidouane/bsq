/*
** mylist.h for mylist.h in /home/el-mou_r/rendu/CPE_2015_bsq/include
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Nov 30 19:47:56 2015 Raidouane EL MOUKHTARI
** Last update Mon Nov 30 19:48:43 2015 Raidouane EL MOUKHTARI
*/

#ifndef MY_LIST_H_
#define MY_LIST_H_

typedef struct	s_list1
{
  int		pos;
  int		cote;
  struct s_list1 *prev;
  struct s_list1 *next;
}		t_list;

#endif
