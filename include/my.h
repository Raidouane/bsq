/*
** my.h for my.h in /home/el-mou_r/rendu/CPE_2015_bsq
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Tue Dec  1 19:33:02 2015 Raidouane EL MOUKHTARI
** Last update Sun Dec 20 20:31:31 2015 Raidouane EL MOUKHTARI
*/

#ifndef _MY_H
# define _MY_H
# include "mylist.h"

typedef struct	s_struct
{
  char		*map;
  int		i;
  int		incr;
  int		line_c;
  int		fd;
  int		inc;
  t_list	*la;
  int		t;
  int		size;
  int		size_find;
  int		line;
  int		col;
  int		k;
  int		c;
  int		l;
  int		nb_square_desired;
  int		width;
  int		pos;
  int		val;
  int		save_l;
  int		pass;
}		t_struct;

typedef	struct	s_struct1
{
  int		i;
  int		l;
  int		pos;
  int		stop;
  char		c;
}		u_struct;

void	my_putchar(char);
void	my_putstr(char *);
int	fs_open_file(char *);
int	get_nbr1(char *str, int a, int na);
int	my_getnbr(char *str);
void	display_map(char *buff, t_list *la, int col, int);
int	check_if_the_map_is_correct(char *buff);
int	manage_read_map(t_struct *bs);
void	init_my_variables(t_struct *bs);
void	do_the_function_check_carre(char *buff, t_struct *bs, int l);
int	check_carre(char *buff, int pos, int l, int larg_l);
void	do_my_principal_algo(t_struct *bs, char *f);
t_list	*bsq(char *f);
t_list	*choose_the_algo(t_struct *bs, char *buff);
void	do_my_boucle_carre(char *buff, int pos, int larg_l, t_struct *bs);
int	check_carre_second(char *buff, int pos, int larg_l, t_struct *bs);
void	do_my_principal_algo_second(t_struct *bs, char *f);
t_list	*bsq_second(char *f, t_struct *bs);
int	check_arguments(int ac,char **av, t_struct *bs);
int	check_option(char *option, char *opt, t_struct *bs);

#endif
