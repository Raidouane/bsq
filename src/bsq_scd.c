/*
** bsq.c for bsq.c in /home/el-mou_r/rendu/CPE_2015_bsq
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Nov 30 16:26:55 2015 Raidouane EL MOUKHTARI
** Last update Sun Dec 20 20:28:55 2015 Raidouane EL MOUKHTARI
*/

#include "../include/mylist.h"
#include "../include/my.h"
#include <stdlib.h>

void	do_my_boucle_carre(char *buff, int pos, int larg_l, t_struct *bs)
{
  bs->size_find = 0;
  bs->incr = pos + (bs->c * larg_l);
  while (buff[bs->incr] == '.' && buff && buff[bs->incr] != '\0')
    {
      bs->size_find++;
      bs->incr++;
    }
  if (bs->size_find <= bs->save_l)
    bs->l = bs->save_l - 2;
  if (bs->size_find >= bs->save_l)
    {
      if (bs->size_find < bs->l)
	bs->l = bs->size_find;
      bs->line_c++;
      bs->c++;
    }
}

int		check_carre_second(char *buff, int pos, int larg_l, t_struct *bs)
{
  bs->incr = 0;
  bs->c = 1;
  bs->line_c = 2;
  if (bs->l > bs->line)
    bs->l = bs->line;
  while (bs->line_c <= bs->l && bs->l > bs->save_l)
    do_my_boucle_carre(buff, pos, larg_l, bs);
  if (bs->size_find >= bs->save_l && bs->l > bs->save_l)
    return (1);
  else
    return (0);
}

void		do_my_principal_algo_second(t_struct *bs, char *f)
{
  t_list	*elem;

  while (f[bs->i] == '.' && bs->pass == 1 && f)
    {
      bs->pos = bs->i;
      while (f[bs->i] == '.')
	{
	  bs->i++;
	  bs->l++;
	}
      while (bs->l > bs->save_l && bs->pass == 1)
	{
	  bs->val = check_carre_second(f, bs->pos, bs->width, bs);
	  if (bs->val == 1)
	    {
	      elem = malloc(sizeof(t_list));
	      elem->pos = bs->pos;
	      elem->cote = bs->l;
	      bs->save_l = bs->l;
	      elem->next = bs->la;
	      bs->la = elem;
	      bs->pass = 0;
	    }
	}
    }
}

t_list		*bsq_second(char *f, t_struct *bs)
{
  init_my_variables(bs);
  while (f && f[bs->width] != '\n')
    bs->width++;
  bs->width++;
  while (f && f[bs->pos] != '\0')
    {
      bs->pass = 1;
      bs->l = 0;
      bs->val = 0;
      while (f[bs->i] == 'o' || f[bs->i] == '\n')
	bs->i++;
      if (f[bs->i] == '\0')
	return (bs->la);
      do_my_principal_algo_second(bs, f);
      bs->i = bs->pos + 1;
    }
}

int		main(int ac, char **av)
{
  t_struct	bs;
  char		c;
  char		*buff;
  t_list	*la;

  bs.inc = 0;
  bs.col = 0;
  check_arguments(ac, av, &bs);
  bs.map = av[1];
  manage_read_map(&bs);
  bs.fd = fs_open_file(av[1]);
  read(bs.fd, &c, 1);
  while (c != '\n')
    read(bs.fd, &c, 1);
  bs.size++;
  if ((buff = malloc(bs.size + 1)) == NULL)
    return (-1);
  read(bs.fd, buff, bs.size);
  buff[bs.size] = '\0';
  check_if_the_map_is_correct(buff);
  la = choose_the_algo(&bs, buff);
  display_map(buff, la, bs.col + 1, bs.nb_square_desired);
  free(buff);
}
