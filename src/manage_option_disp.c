/*
** manage_option_disp.c for manage_option_disp.c in /home/el-mou_r/rendu/CPE_2015_BSQ/verd/CPE_2015_BSQ
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sat Dec 19 14:16:30 2015 Raidouane EL MOUKHTARI
** Last update Sat Dec 19 19:56:20 2015 Raidouane EL MOUKHTARI
*/

#include "../include/mylist.h"
#include "../include/my.h"
#include <stdlib.h>

void	do_my_display_map(u_struct *bs, char **buff, t_list *la, int col)
{
  while (bs->stop < la->cote)
    {
      while ((*buff)[bs->i] != '\0' && *buff && bs->l < la->cote)
        {
          (*buff)[bs->i] = bs->c;
          bs->i++;
          bs->l++;
        }
      bs->pos = bs->pos + col;
      bs->l = 0;
      bs->i = bs->pos;
      bs->stop++;
    }
}

int	init_variables_to_disp(int save_pos, u_struct *bs, char *mod, int u)
{
  save_pos = bs->pos;
  bs->l = 0;
  bs->stop = 0;
  bs->i = bs->pos;
  bs->c = mod[u];
  return (save_pos);
}

void		display_map(char *buff, t_list *la, int col, int nb_square_desired)
{
  u_struct      bs;
  int           save_pos;
  char          *mod;
  int           u;

  u = 0;
  mod = malloc(13);
  mod = "x*$%@/#m?§!^";
  while (nb_square_desired > 0)
    {
      bs.pos = la->pos;
      save_pos = init_variables_to_disp(save_pos, &bs, mod, u);
      if (la->cote == 1)
        nb_square_desired = 0;
      do_my_display_map(&bs, &buff, la, col);
      if (la->next != NULL)
        la = la->next;
      while (la->pos == save_pos && la->next != NULL)
        la = la->next;
      u++;
      if (u == '\0')
        u = 0;
      nb_square_desired--;
    }
  my_putstr(buff);
}
