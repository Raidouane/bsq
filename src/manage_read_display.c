/*
** manage_read_display.c for manage_read_display.c in /home/el-mou_r/rendu/CPE_2015_BSQ
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
**
** Started on  Mon Dec 14 17:51:14 2015 Raidouane EL MOUKHTARI
** Last update Sun Dec 20 20:27:32 2015 Raidouane EL MOUKHTARI
*/

#include "../include/mylist.h"
#include "../include/my.h"
#include <stdlib.h>

int	check_if_the_map_is_correct(char *buff)
{
  int	i;
  int	size;

  i = 0;
  while (buff[i] != '\0' && buff)
    i++;
  size = i;
  i = 0;
  while (buff && (buff[i] == 'o' || buff[i] == '.' || buff[i] == '\n'))
    i++;
  if (i == size)
    {
      i = 0;
      while (buff && (buff[i] == 'o' || buff[i] == '\n') && buff[i] != '\0')
        i++;
      if (i == size)
        {
          my_putstr(buff);
          exit (1);
        }
      else
        return (0);
    }
  else
    exit (1);
}

int	manage_read_map(t_struct *bs)
{
  char	c;
  char	*buff;

  bs->fd = fs_open_file(bs->map);
  read(bs->fd, &c, 1);
  while (c != '\n')
    {
      read(bs->fd, &c, 1);
      bs->inc++;
    }
  bs->fd = fs_open_file(bs->map);
  if ((buff = malloc(bs->inc + 1)) == NULL)
    exit (-1);
  read(bs->fd, buff, bs->inc);
  buff[bs->inc] = '\0';
  bs->line = my_getnbr(buff);
  read(bs->fd, &c, 1);
  read(bs->fd, &c, 1);
  while (c != '\n')
    {
      read(bs->fd, &c, 1);
      bs->col++;
    }
  bs->size = (bs->line * (bs->col + 1)) - 1;
  free(buff);
}

void	init_my_variables(t_struct *bs)
{
  bs->i = 0;
  bs->pos = 0;
  bs->l = 0;
  bs->pass = 1;
  bs->la = NULL;
  bs->val = 0;
  bs->save_l = 0;
  bs->width = 0;
}
