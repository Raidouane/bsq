/*
** bsq.c for bsq.c in /home/el-mou_r/rendu/CPE_2015_bsq
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Nov 30 16:26:55 2015 Raidouane EL MOUKHTARI
** Last update Sat Dec 19 02:11:55 2015 Raidouane EL MOUKHTARI
*/

#include "../include/mylist.h"
#include "../include/my.h"
#include <stdlib.h>

void	do_the_function_check_carre(char *buff, t_struct *bs, int l)
{
  while (bs->k < l && buff[bs->i] == '.' && bs->pass == 1)
    {
      bs->i++;
      bs->k++;
    }
  if (bs->k == l && bs->c < l && bs->pass == 1)
    {
      bs->c++;
      bs->pass = 1;
    }
  else
    bs->pass = 0;
}

int		check_carre(char *buff, int pos, int l, int larg_l)
{
  t_struct	bs;

  bs.c = 1;
  bs.pass = 1;
  while (buff && bs.pass == 1)
    {
      bs.k = 0;
      bs.i = pos + (bs.c * larg_l);
      if (bs.c == l)
	bs.pass = 0;
      if (buff[bs.i] == 'o' && bs.pass == 1)
	return (0);
      do_the_function_check_carre(buff, &bs, l);
    }
  if (bs.c == l)
    return (1);
  else
    return (0);
}

void		do_my_principal_algo(t_struct *bs, char *f)
{
  t_list	*elem;

  while (f[bs->i] == '.' && bs->pass == 1 && f)
    {
      bs->pos = bs->i;
      while (f[bs->i] == '.')
	{
	  bs->i++;
	  bs->l++;
	}
      while (bs->t <= bs->l && bs->l > bs->save_l && bs->pass == 1)
	{
	  bs->val = check_carre(f, bs->pos, bs->t, bs->width);
	  if (bs->val == 1 && bs->t > bs->save_l)
	    {
	      elem = malloc(sizeof(t_list));
	      elem->pos = bs->pos;
	      elem->cote = bs->t;
	      bs->save_l = bs->t;
	      elem->next = bs->la;
	      bs->la = elem;
	    }
	  bs->t++;
	}
    }
}

t_list		*bsq(char *f)
{
  t_struct	bs;

  init_my_variables(&bs);
  while (f && f[bs.width] != '\n')
    bs.width++;
  bs.width++;
  while (f && f[bs.pos] != '\0')
    {
      bs.pass = 1;
      bs.l = 0;
      bs.val = 0;
      bs.t = 1;
      while (f[bs.i] == 'o' || f[bs.i] == '\n')
	bs.i++;
      if (f[bs.i] == '\0')
	return (bs.la);
      do_my_principal_algo(&bs, f);
      bs.i = bs.pos + 1;
    }
}
