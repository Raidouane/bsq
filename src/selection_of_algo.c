/*
** selection_of_algo.c for selection_of_algo.c in /home/el-mou_r/rendu/CPE_2015_BSQ/verd/CPE_2015_BSQ
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sat Dec 19 02:20:02 2015 Raidouane EL MOUKHTARI
** Last update Sun Dec 20 20:30:18 2015 Raidouane EL MOUKHTARI
*/

#include "../include/mylist.h"
#include "../include/my.h"
#include <stdlib.h>

int	check_option(char *option, char *opt, t_struct *bs)
{
  int	i;
  int	t;

  i = 0;
  t = 0;
  if ((opt = malloc(sizeof(char) * 10)) == NULL)
    exit(-1);
  while (option[i] == '-' || option[i] == 'c')
    i++;
  while (option && option[i]!= '\0')
    {
      opt[t] = option[i];
      i++;
      t++;
    }
  opt[t] = '\0';
  bs->nb_square_desired = my_getnbr(opt);
}

int	check_arguments(int ac,char **av, t_struct *bs)
{
  char	*opt;
  char	*option;

  bs->nb_square_desired = 1;
  if (ac == 3)
    {
      if (av[2][0] == '-' && av[2][1] == 'c')
	{
	  option = av[2];
	  check_option(option, opt, bs);
	}
      else
	exit (-1);
    }
  if (ac > 3)
    exit (-1);
}

t_list		*choose_the_algo(t_struct *bs, char *buff)
{
  t_list	*la;

  if (bs->line > 200 && bs->col > 2)
    la = bsq_second(buff, bs);
  else
    la = bsq(buff);
  return (la);
}
