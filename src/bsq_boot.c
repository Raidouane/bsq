/*
** bsq_boot.c for bsq_boot.c in /home/el-mou_r/map_bsq/CPE_2015_BSQ_bootstrap
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Tue Nov 24 16:25:23 2015 Raidouane EL MOUKHTARI
** Last update Mon Dec 14 17:47:56 2015 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include <fcntl.h>

int	fs_open_file(char *filepath)
{
  int	fd;

  fd = open(filepath, O_RDONLY);
  if (fd == -1)
    exit(-1);
  return (fd);
}
