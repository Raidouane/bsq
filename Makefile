##
## Makefile for Makefile in /home/el-mou_r/rendu/CPE_2015_BSQ
## 
## Made by Raidouane EL MOUKHTARI
## Login   <el-mou_r@epitech.net>
## 
## Started on  Wed Dec  9 02:26:38 2015 Raidouane EL MOUKHTARI
## Last update Sun Dec 20 20:10:02 2015 Raidouane EL MOUKHTARI
##

CC	= gcc

RM	= rm

NAME	= bsq

CFLAGS	= -I./include

SRC	=src/bsq.c			\
	src/bsq_scd.c			\
	src/bsq_boot.c			\
	src/manage_read_display.c	\
	src/manage_option_disp.c	\
	src/selection_of_algo.c		\
	src/my_getnbr.c			\
	src/my_putchar.c		\
	src/my_putstr.c

OBJ	= $(SRC:.c=.o)

$(NAME): $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $(NAME)

all:	$(NAME)

clean:
	$(RM) -f $(OBJ)

fclean:	clean
	$(RM) -f $(NAME)

re:	fclean all
